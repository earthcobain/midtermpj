package buu.informatics.s59160106.midtermpj

class Score(var correct: Int = 0, var wrong: Int = 0){
    fun addCorrect(){
        correct++
    }
    fun addWrong(){
        wrong++
    }
}