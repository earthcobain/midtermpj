package buu.informatics.s59160106.midtermpj

import kotlin.random.Random

class Question (
    var number1: Int = 0,
    var number2: Int = 0,
    var numberResult:Int = 0,
    var choice1: Int = 0,
    var choice2: Int = 0,
    var choice3:Int = 0,
    var msgResult :String = ""
) {
    fun checkAnswerCorrect(choiceNumber:Int) : Boolean{
        var answer: Int = when (choiceNumber) {
            1 -> choice1
            2 -> choice2
            3 -> choice3
            else -> 0
        }
        return answer === numberResult
    }
    fun createQuestionAndResultOfSum() {
        this.number1 = Random.nextInt(1, 10)
        this.number2 = Random.nextInt(1, 10)
        this.numberResult = number1 + number2
    }
    fun createQuestionAndResultOfSub() {
        this.number1 = Random.nextInt(1, 10)
        this.number2 = Random.nextInt(1, 10)
        this.numberResult = number1 - number2
    }
    fun createQuestionAndResultOfMuti() {
        this.number1 = Random.nextInt(1, 10)
        this.number2 = Random.nextInt(1, 10)
        this.numberResult = number1 * number2
    }

    fun createChoice(){
        val position = Random.nextInt(1, 4)
        if (position === 1) {
            this.choice1 = numberResult
            this.choice2  = numberResult + 1
            this.choice3  = numberResult + 2
        } else if (position === 2) {
            this.choice1 = numberResult - 1
            this.choice2  = numberResult
            this.choice3  = numberResult + 1
        } else if (position === 3) {
            this.choice1 = numberResult - 2
            this.choice2  = numberResult - 1
            this.choice3  = numberResult
        }
    }
}