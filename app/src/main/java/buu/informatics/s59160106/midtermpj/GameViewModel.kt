package buu.informatics.s59160106.midtermpj

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

class GameViewModel : ViewModel(){
    private val _score = MutableLiveData<Score>()
    val score : LiveData<Score>
    get() =_score

    private val _question = MutableLiveData<Question>()
    val question : LiveData<Question>
    get() = _question

    private val _isAddScore = MutableLiveData<Boolean>()
    val isAddScore : LiveData<Boolean>
    get() = _isAddScore

    private val _eventNewGame = MutableLiveData<Boolean>()
    val eventNewGame : LiveData<Boolean>
        get() = _eventNewGame

    private val _eventChooseAnswer1 = MutableLiveData<Boolean>()
    val eventChooseAnswer1 : LiveData<Boolean>
        get() = _eventChooseAnswer1

    private val _eventChooseAnswer2 = MutableLiveData<Boolean>()
    val eventChooseAnswer2 : LiveData<Boolean>
        get() = _eventChooseAnswer2

    private val _eventChooseAnswer3 = MutableLiveData<Boolean>()
    val eventChooseAnswer3 : LiveData<Boolean>
        get() = _eventChooseAnswer3

    private val _eventUpdateResult = MutableLiveData<Int>()
    val eventUpdateResult : LiveData<Int>
        get() = _eventUpdateResult


    private val _questionTime = MutableLiveData<Long>()
    val questionTime : LiveData<Long>
        get() = _questionTime

    private var questionTimer: CountDownTimer?

    val currentQuestionTimeString = Transformations.map(questionTime) { time ->
        _questionTime.value?.toInt()
    }
    fun getScoreValue() = _score.value
    fun getQuestionValue() = _question.value

    fun setIsAddScore(isAdd:Boolean){
        _isAddScore.value = isAdd
    }
    fun onChooseAnswer1() {
        updateResult(1)
        _eventChooseAnswer1.value = true
    }

    fun onChooseAnswer2() {
        updateResult(2)
        _eventChooseAnswer2.value = true
    }
    fun onChooseAnswer3() {
        updateResult(3)
        _eventChooseAnswer3.value = true
    }

    fun onResultCorrect() {
        _eventUpdateResult.value = 1
    }
    fun onResultCorrectNotAdd() {
        _eventUpdateResult.value = 2
    }
    fun onResultWrong() {
        _eventUpdateResult.value = 3
    }

    fun updateResult(choiceNumber:Int){
        val question = getQuestionValue()
        if (question?.checkAnswerCorrect(choiceNumber)!!) {
            if (checkCanAddScore()) {
                addAmountCorrect()
                onResultCorrect()
            }else{
                onResultCorrectNotAdd()
            }
        } else {
            if (checkCanAddScore()) {
                addAmountWrong()
                onResultWrong()
            }
        }
        _isAddScore.value = true
    }
    fun checkCanAddScore() : Boolean{
        return !isAddScore.value!!
    }
    fun onChooseComplete() {
        _eventChooseAnswer1.value = false
        _eventChooseAnswer2.value = false
        _eventChooseAnswer3.value = false
    }

    fun startTimeQuestion() {
        questionTimer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                _questionTime.value = millisUntilFinished / 1000L
            }
            override fun onFinish() {
                _questionTime.value = 0L
                _question.value?.msgResult = ""
                newQuestion()
            }
        }
        questionTimer?.start()
    }
    fun stopTimeQuestion() {
        _questionTime.value = 0L
        questionTimer?.cancel()
    }

    fun addAmountCorrect() {
        if(!_isAddScore.value!!){
            _score.value?.addCorrect()
        }
        _isAddScore.value = true
    }

    fun addAmountWrong() {
        _score.value?.addWrong()
        _isAddScore.value = true
    }

    fun newQuestion(){
        _question.value?.createQuestionAndResult(_menu.value!!)
        _question.value?.createChoice()
        _isAddScore.value = false
        _eventNewGame.value = true
    }
    fun newGameComplete(){
        _eventNewGame.value = false
    }
    fun setMenu(menu:Int){
        _menu.value = menu
    }

    init {
        questionTimer = null
        _menu.value = 0
        _score.value = Score()
        _question.value = Question()
        _isAddScore.value = false
    }
}