package buu.informatics.s59160106.midtermpj

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import buu.informatics.s59160106.midtermpj.databinding.FragmentSumBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SumFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SumFragment : Fragment() {
    lateinit var binding : FragmentSumBinding

    private var question: Question = Question()
    private var score: Score = Score()

    private var isAddScore :Boolean = false



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentSumBinding>(
            inflater,
            R.layout.fragment_sum, container, false
        )
        binding.score = score
        binding.question = question

        mainGame()
        setBtnBackToMain()
        return binding.root
    }
    private fun mainGame() {
        isAddScore = false
        question.createQuestionAndResultOfSum()
        question.createChoice()
        resetComponent()
        resetBackground()
        binding.invalidateAll()
    }



//    private fun getScore(){
//        val args = SumFragmentArgs.fromBundle(requireArguments())
//        score.correct = args.amoutCorrect
//        score.wrong = args.amoutWrong
//    }

    private fun resetComponent(){
        question.msgResult = ""

        binding.apply {
            txtTime.text = ""
            txtNumber1.visibility = View.VISIBLE
            txtNumber2.visibility = View.VISIBLE
            txtOperator.visibility = View.VISIBLE

            btnAnswer1.setOnClickListener{
                onSelectAnswer(1,it)
            }
            btnAnswer2.setOnClickListener{
                onSelectAnswer(2,it)
            }
            btnAnswer3.setOnClickListener{
                onSelectAnswer(3,it)
            }
        }
        enableAllBtn()
    }
    private fun enableAllBtn(){
        binding.apply {
            btnAnswer1.setEnabled(true)
            btnAnswer2.setEnabled(true)
            btnAnswer3.setEnabled(true)
            btnBackSumToMain.setEnabled(true)
        }
    }
    private fun disableAllBtn(){
        binding.apply {
            btnAnswer1.setEnabled(false)
            btnAnswer2.setEnabled(false)
            btnAnswer3.setEnabled(false)
            btnBackSumToMain.setEnabled(false)
        }
    }

    private fun addAmountCorrect(){
        score.addCorrect()
        isAddScore = true
        binding.invalidateAll()
    }
    private fun addAmountWrong(){
        score.addWrong()
        isAddScore = true
        binding.invalidateAll()
    }
    private fun resetBackground(){
        binding.apply {
            txtNumberResult.setTextColor(Color.BLACK)
            btnAnswer1.setBackgroundColor( resources.getColor(R.color.buttonInit))
            btnAnswer2.setBackgroundColor( resources.getColor(R.color.buttonInit))
            btnAnswer3.setBackgroundColor( resources.getColor(R.color.buttonInit))
        }
    }
    private fun changeBackgroundWrong(btn: Button){
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.wrong))
//        btn.setBackgroundColor( resources.getColor(R.color.wrong))
        btn.setBackgroundColor(Color.RED)
    }
    private fun changeBackgroundCorrect(btn: Button){
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.correct))
//        btn.setBackgroundColor( resources.getColor(R.color.correct))
        btn.setBackgroundColor(Color.GREEN)
    }

    fun onSelectAnswer(choiceNumber:Int,view: View){
        val btn = (view as Button)
        var answer : Int = when(choiceNumber){
            1 -> question.choice1
            2 -> question.choice2
            3 -> question.choice3
            else -> 0
        }

        if(answer === question.numberResult ){
            question.msgResult = "ถูกต้อง แต่ไม่เพิ่มคะแนน"
            if(!isAddScore){
                question.msgResult = "ถูกต้อง"
                addAmountCorrect()
            }
            disableAllBtn()
            changeBackgroundCorrect(btn)
            contDownForNewGame()
        }else{
            question.msgResult = "${btn.text} เลือกใหม่นะ"
            if(!isAddScore){
                addAmountWrong()
            }
            changeBackgroundWrong(btn)
        }
        btn.setEnabled(false)
        binding.invalidateAll()
    }
    fun contDownForNewGame(){
        binding.apply {
            txtNumber1.visibility = View.INVISIBLE
            txtNumber2.visibility = View.INVISIBLE
            txtOperator.visibility = View.INVISIBLE
        }
        val timer = object: CountDownTimer(1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
//                binding.txtTime.text = "ข้อต่อไป"
            }
            override fun onFinish() {
                mainGame()
            }
        }
        timer.start()
    }

    fun setBtnBackToMain(){

        binding.btnBackSumToMain.setOnClickListener{
            it.findNavController().navigate(SumFragmentDirections.actionSumFragmentToMenuFragment())
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            val navController = this@SumFragment.findNavController()
            navController.navigate(SumFragmentDirections.actionSumFragmentToMenuFragment())
        }
    }



}